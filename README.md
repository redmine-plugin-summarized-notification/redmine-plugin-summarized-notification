# Redmine plugin summarized notification

A Redmine plugin to send a summarized notification per day per project
instead of sending notifications immediately.

## Install

Install this plugin:

```bash
cd redmine
git clone https://gitlab.com/redmine-plugin-summarized-notification/redmine-plugin-summarized-notification.git plugins/summarized_notification
```

Restart Redmine.

### Register periodical task

You need to send summarized notifications daily.

For GNU/Linux:

Install systemd service to run `summarized_notification:send` daily:

```bash
cd redmine
RAILS_ENV=production plugins/summarized_notification/bin/generate_systemd_service | \
  sudo -H tee /lib/systemd/system/redmine-summarized-notification.service
RAILS_ENV=production plugins/summarized_notification/bin/generate_systemd_timer | \
  sudo -H tee /lib/systemd/system/redmine-summarized-notification.timer
sudo -H systemctl daemon-reload
sudo -H systemctl enable --now redmine-summarized-notification.timer
```

For Windows:

You can use Task Scheduler to run `summarized_notification:send` daily.

TODO: How to do this?

## Uninstall

Uninstall systemd service:

```bash
sudo -H systemctl disable --now redmine-summarized-notification.timer
sudo -H rm /lib/systemd/system/redmine-summarized-notification.timer
sudo -H rm /lib/systemd/system/redmine-summarized-notification.service
sudo -H systemctl daemon-reload
```

Uninstall this plugin:

```bash
cd redmine
rm -rf plugins/summarized_notification
```

Restart Redmine.

## Usage

Each user can enable/disable summarized notification on his/her
`/my/account` page. You can find a new "Summarize notification"
configuration item.

## Authors

  * Sutou Kouhei `<kou@clear-code.com>`

## License

GPLv2 or later. See [LICENSE](LICENSE) for details.

