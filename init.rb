# Copyright (C) 2021-2022  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Redmine::Plugin.register :summaried_notification do
  name "Summarized notification plugin"
  author "Sutou Kouhei"
  description "This is a Redmine plugin to summarize notification"
  version "1.0.0"
  url "https://gitlab.com/redmine-plugin-summarized-notification/redmine-plugin-summarized-notification"
  author_url "https://gitlab.com/redmine-plugin-summarized-notification"
  directory __dir__
end

prepare = lambda do
  # Load
  SummarizedNotification::MailerMixin
  SummarizedNotification::UserPreferenceMixin
end

# We need to initialize explicitly with Redmine 5.0 or later.
prepare.call if Redmine.const_defined?(:PluginLoader)

Rails.configuration.to_prepare(&prepare)
