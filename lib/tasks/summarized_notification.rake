# Copyright (C) 2021  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

namespace :summarized_notification do
  desc <<-DESCRIPTION
Send summaries in the target date.

Available options:
  * target_date: Collect change in the target date. (default: yesterday)

Example: Send summarized notifications for yesterday
  RAILS_ENV=production bin/rails summarized_notification:send

Example: Send summarized notifications for 2021-01-28
  RAILS_ENV=production bin/rails summarized_notification:send target_date=2021-01-28
  DESCRIPTION
  task :send => :environment do
    options = {}
    options[:target_date] = ENV["target_date"].presence
    if options[:target_date]
      options[:target_date] = Time.zone.parse(options[:target_date])
    else
      options[:target_date] = Time.zone.now.yesterday
    end
    Mailer.with_synched_deliveries do
      Mailer.summaries(**options)
    end
  end
end
