# Copyright (C) 2021-2022  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

module SummarizedNotification
  module MailerMixin
    class << self
      def prepended(base)
        base.extend(ClassMethods)
      end
    end

    module ClassMethods
      def summaries(target_date: Time.zone.now)
        target_start = target_date.beginning_of_day
        target_end = target_date.end_of_day
        target_range = target_start..target_end
        target_date_text = target_date.strftime("%Y-%m-%d")
        target_preferences =
          UserPreference.
            where("others LIKE ?", "%:summarize_notification: '1'%")
        User.where(id: target_preferences.select("user_id")).find_each do |user|
          Project.project_tree(user.projects) do |project, n_ancestors|
            next if n_ancestors > 0
            issues = change_current_user(user) do
              query = IssueQuery.new
              query.name = "summarize"
              query.user = user
              query.project = project
              query.add_filter("updated_on", "=", [target_date_text])
              query.add_filter("subproject_id", "*")
              query.issues
            end
            next if issues.empty?
            summary(user,
                    project,
                    issues,
                    target_date_text).
              deliver_later
          end
        end
      end

      private
      def change_current_user(user)
        current_user = User.current
        begin
          User.current = user
          yield
        ensure
          User.current = current_user
        end
      end
    end

    def mail(headers={}, &block)
      to = headers[:to]
      if to.is_a?(Principal) and to.pref.summarize_notification?
        self.message = ActionMailer::Base::NullMail.new
        return message
      end
      super
    end

    def summary(user, project, issues, target_date_text)
      @user = user
      @project = project
      @issues = issues
      @issues_url = url_for(controller: 'issues',
                            action: 'index',
                            project_id: @project,
                            set_filter: 1,
                            f: ["updated_on"],
                            op: {updated_on: "="},
                            v: {updated_on: [target_date_text]})
      # Bypass summarize_notification check by using email address.
      mail to: @user.mail,
           subject: l(:mail_subject_summary,
                      project: @project.name,
                      n_issues: @issues.size)
    end
  end
end

Mailer.prepend(SummarizedNotification::MailerMixin)
